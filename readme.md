# Goproxy, 简单灵活的反向代理和静态资源代理 #

## 软件下载 ##

[goproxy代理下载](https://bitbucket.org/weager/goproxy/downloads)  （由于bitbucket基本属于半被墙状态，下载功能必须使用代理才能下载，建议大家使用收费代理或者goAgent免费代理）
该软件的开发缘由，请看我的一篇博文[Goproxy-灵活的反向代理和静态资源代理](http://rongmayisheng.com/post/goproxy-灵活的反向代理和静态资源代理)

其他下载地址：

1. windows64位：[goproxy－windows－64－0.0.3.exe](http://www.kuaipan.cn/file/id_862992073767372.htm)
2. windows32位：[goproxy－windows－32－0.0.3.exe](http://www.kuaipan.cn/file/id_862992073767371.htm)

## 软件介绍 ##
1. Goproxy使用代理的方式来加强hosts文件的配置，对hosts文件的修改实时生效(不需要使用firefox上的dns flusher插件)，对hosts文件里正常的配置没有任何影响。
2. 通过配置hosts文件中的dns映射，可实现**自动替换**url的前缀，**添加**或者**移除**指定的**参数**。
3. 配合上[switchhosts](https://github.com/oldj/SwitchHosts) 等hosts dns切换软件，特别适合web开发时让浏览器快速的切换线上、线下、测试环境。（chrome上没有类似firefox上的dnsflusher插件，有了goproxy你就有福了）
4. Goproxy也可以作为普通的代理软件来代理HTTP请求，或者代理本地静态资源文件（图片、js、css等，可通过简单的配置取代varnish+apache）。
5. 无需安装，单个exe文件，双击即可运行，跨平台，无额外的配置文件（只需要配置hosts文件即可，当然也可以自行制定配置文件）
直接修改hosts文件，利用丰富的前缀匹配解决线上线下的环境访问的问题。
建议使用傲游等对代理配置很灵活的浏览器（可以仅对某几个域名使用代理）。

-------------------------------------------------------------------------------------------

## 使用介绍 ##

启动goproxy时，用如下的方式可以查看帮助  
`goproxy.exe -help`

**代理程序的端口号**

在浏览器中配置代理时，默认端口号为8000，如果想使用其他端口号，请在启动goproxy的时候，使用`-port=xxx`启动参数来修改端口号。比如：

`goproxy.exe -port=8080`

**使用其他配置文件**

推荐直接修改C:\Windows\System32\drivers\etc\hosts，因为现在已经有不少软件能很好的直接这个文件修改，比如[switchhosts](https://github.com/oldj/SwitchHosts) 如果你不想修改host文件，而使用其他文件来配置映射关系，也可以通过`-hosts=C:\\myconfig.conf`来指定其他位置的配置文件。比如：

`goproxy.exe -hosts=C:\\myconfig.conf`

**hosts文件配置**

hosts文件的配置是为了让goproxy代理程序知道需要对哪些url前缀进行替换，哪些参数需要增删。你可以把hosts文件配置成这样（当然如果你指定的是其他文件，就把dns映射的配置写到你指定的配置文件中，就不需要修改hosts文件了）  

`127.0.0.1:8080/app/sea  www.baidu.com/search/xxx  +pageSize=30  -beginPage`  
`D:/workspace/work/style  style.china.alibaba.com/app/search`  

这行配置的每一项意思是：  
`最终的url路径前缀  用于匹配的url路径前缀  需要添加的参数  需要移除的参数`

第一行配置相当于把url中“www.baidu.com/search/xxx”前缀替换为“127.0.0.1:8080/app/sea”，并给替换后的url添加参数pageSize=30,并移除参数beginPage及其参数值。  
第二行配置是指把http://style.china.alibaba.com/app/search前缀开头的url替换为D:/workspace/work/style前缀，并读取本地文件返回给浏览器。  
当然普通的host dns映射配置和注释都是无缝的支持的，因为操作系统对这些奇怪的配置忽略掉了，对正常的配置仍然是生效的。比如你的chrome配置了代理指向goproxy，那么hosts文件中的所有配置都会有效，而你的firefox没有配置代理，那么firefox会使用hosts中的正常的dns映射，以上奇怪的映射会被忽略，不会影响firefox的正常使用。

**hosts文件配置高级用法**

goproxy的原理其实很简单，就是替换前缀，增减参数，它是按照配置文件中配置的顺序，从第一条开始匹配，一旦匹配上，就替换前缀得到替换后的新url，然后用新url访问；如果都没匹配上，则对url不做任何修改直接访问。所以如果一个url在hosts映射配置中有两个映射配置都符合时，出现在配置文件前面的会被使用。

`D:/workspace/work/style  style.china.alibaba.com/app/search`  
`10.20.111.10  style.china.alibaba.com`  
`www.1688.com  www.1688.com  +debug=true`  

第一行和第二行有相同的域名，如果url的前缀为`style.china.alibaba.com/app/search`，则会匹配上第一行的映射，第二行不会被匹配上；如果url的前缀为`style.china.alibaba.com\aaa`,则会匹配上第二行的映射，匹配后的url为10.20.111.10\aaa。  
第三行的配置显得有点奇怪，最终的url路径前缀和用于匹配的url前缀相同，这样做只是为了让www.1688.com前缀的url都加上debug=true的参数而已（匹配上了才会可以增减参数，所以此处是巧妙运用了一下），如果不这样配置，goproxy是不会知道这个url需要加参数的。

-------------------------------------------------------------------------------------------

## TODO List ##
1. 对静态资源和动态资源进行cache，并在hosts文件中可使用参数配置。
2. 提供web界面来管理（增删改查）hosts映射，不依赖于其他hosts切换软件
3. 提供一个浏览器代理自动配置的pac脚本的编辑和查看页面。

-------------------------------------------------------------------------------------------

## 不足之处 ##
1. 不能代理HTTPS请求

-------------------------------------------------------------------------------------------

## License ##
Goproxy is licensed under the Apache Licence, Version 2.0(http://www.apache.org/licenses/LICENSE-2.0.html).

-------------------------------------------------------------------------------------------