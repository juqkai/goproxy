/*
使用代理的方式来加强hosts文件的配置，可以替换url的前缀，并添加或者移除指定的参数。特别适合web开发时使用。
直接修改hosts文件，利用丰富的前缀匹配解决线上线下的一些环境切换的问题。对hosts文件里正常的配置没有任何影响。
建议使用傲游等对代理配置很灵活的浏览器（可以仅对某几个域名使用代理）。
比如：127.0.0.1:8080/app/sea  www.baidu.com/search/xxx  +pageSize=30  -beginPage
                最终的url路径前缀                用于匹配的url路径前缀                   需要添加的参数        需要移除的参数
这个例子意思是：把url中“www.baidu.com/search/xxx”前缀替换为“127.0.0.1:8080/app/sea”，
并给每个匹配“www.baidu.com/search/xxx”前缀的url添加参数pageSize=30,并移除参数beginPage及其参数值。
*/

package main

import (
	utils "bitbucket.org/weager/utils"
	"bufio"
	"flag"
	"fmt"
	"log"
	http "net/http"
	httputil "net/http/httputil"
	url "net/url"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"
)

/*
url转换映射，并且可以定制需要添加的参数和需要移除的参数。
比如：127.0.0.1:8080/app/sea  www.baidu.com/search/xxx  +pageSize=30  -beginPage
                最终的url路径前缀                用于匹配的url路径前缀                   需要添加的参数        需要移除的参数
*/
type Routing struct {
	path           string     // 最终的url路径前缀
	patterns       []string   // 用于匹配的url路径前缀
	commands       []*Command // 需要执行的参数操作（添加和删除，分别对应+和-），可以有多个，多个参数操作间用空格隔开，格式为：+参数名1=参数值1 -参数名2=参数值2 -参数名3
	isReverseProxy bool       // 是否是反向代理，如果不是则是代理静态资源
}

type Command struct {
	op         func(string, string, *url.URL) //操作符，比如：+、-，分别表示添加和删除
	paramName  string                         //参数名
	paramValue string                         //参数值
}

func (c *Command) do(url *url.URL) {
	c.op(c.paramName, c.paramValue, url)
}

var routings []*Routing    // 全局的url转换映射
var hostsVersion time.Time // hosts文件的最后修改时间

// 域名统计信息
type DomainStatistic struct {
	count         int   // 域名在所有用于匹配的url路径前缀中出现的次数
	routingsIndex []int // 域名在routings中的哪些Routings中出现过
}

// 根据url中的域名来快速判断这个url是否能由一个routing与之匹配，同时保证匹配顺序与配置文件中映射配置顺序一致
var domains map[string]*DomainStatistic = make(map[string]*DomainStatistic) // key为用于匹配的url前缀中的域名，value为该域名出现的次数。

// hosts文件的绝对路径，比如：C:\\Windows\\System32\\drivers\\etc\\hosts
var hosts *string = flag.String("hosts", "C:\\Windows\\System32\\drivers\\etc\\hosts",
	`this is the default value.
	you can specify the hosts configuration to other file, for example,
	linux: "-hosts=/etc/hosts"
	windows: "-hosts=C:\\Windows\\System32\\drivers\\etc\\hosts"`)

/// 代理的端口号，默认端口号为8000
var port *int = flag.Int("port", 8000, `this is the default value. you can set the listening port to 8080, for example: "-port=8080"`)

var debug *bool = flag.Bool("debug", false, `this is the default value. you can open the debug mode to check the log in console, for example: "-debug=true"`)

func main() {
	flag.Usage = func() {
		detail := `please go to the web site "https://bitbucket.org/weager/goproxy/overview" to get more detail`
		log.Printf("%s\nUsage of %s:\n", detail, os.Args[0])
		flag.PrintDefaults()
	}
	flag.Parse()

	log.SetPrefix("[INFO]")

	// 读取hosts文件，解析成url转换映射
	routings = readHostsFile(*hosts)
	http.HandleFunc("/", handerAll) 
	binding := "0.0.0.0:" + strconv.Itoa(*port)
	runtime.GOMAXPROCS(runtime.NumCPU())
	log.Printf("goproxy started on 0.0.0.0%s", binding)
	err := http.ListenAndServe(binding, nil)
	if err != nil {
		log.Panicln("[ERROR] ERROR: %s", err)
	}
}

// 读取hosts文件并解析成url转换关系映射
func readHostsFile(f string) (routings []*Routing) {
	log.Printf("loading hosts file:%s", f)
	file, modTime, err := getFile(f)
	hostsVersion = modTime
	defer file.Close()
	if err != nil {
		log.Panicln("[ERROR] read hosts error:", err)
	}

	bufReader := bufio.NewReader(file)
	lineNum := -1
	for {
		line, err := utils.ReadLine(bufReader)
		if err != nil {
			break
		}
		// 如果这一行是注释，则跳到下一行继续解析
		if strings.HasPrefix(line, "#") {
			continue
		}
		lineNum++
		line = strings.Trim(line, " \t")
		line = strings.Replace(line, "\t", " ", 100)
		segments := utils.Split(line, " ")
		if len(segments) > 1 {
			var path = segments[0] // 最终的url路径前缀
			var patterns []string
			var commands []*Command
			for i := 1; i < len(segments); i++ {
				switch {
				case strings.HasPrefix(segments[i], "-") && len(segments[i]) > 1: // 要移除的参数
					paramSeg := strings.Split(segments[i][1:len(segments[i])], "=")
					if len(paramSeg) == 2 {
						commands = append(commands, &Command{delParam, paramSeg[0], paramSeg[1]})
					} else {
						commands = append(commands, &Command{delParam, paramSeg[0], ""})
					}
				case strings.HasPrefix(segments[i], "+") && strings.Contains(segments[i], "=") && len(segments[i]) > 2: // 要添加的参数
					paramSeg := strings.Split(segments[i][1:len(segments[i])], "=")
					if len(paramSeg) == 2 {
						commands = append(commands, &Command{addParam, paramSeg[0], paramSeg[1]})
					} else {
						commands = append(commands, &Command{addParam, paramSeg[0], ""})
					}
				default:
					patterns = append(patterns, segments[i]) // 用于匹配的url路径前缀
					domain := extractDomain(segments[i])
					domainStatistic := domains[domain]
					if domainStatistic == nil {
						domains[domain] = &DomainStatistic{1, []int{lineNum}}
					} else {
						domainStatistic.count++
						domainStatistic.routingsIndex = append(domainStatistic.routingsIndex, lineNum)
					}
				}
			}
			routings = append(routings, &Routing{path, patterns, commands, isReverseProxy(path)})
		}
	}
	printRoutings(routings)
	return
}

func extractDomain(str string) string {
	//	if strings.HasPrefix(str, "http://") {
	//		str = str[7:len(str)]
	//	}

	if slashIndex := strings.Index(str, "/"); slashIndex != -1 {
		str = str[0:slashIndex]
	}
	return str
}

func printRoutings(routings []*Routing) {
	//  打印出所有的url转换映射
	for _, routing := range routings {
		fmt.Print("[INFO] routings:\n path:", routing.path, ", patterns:", routing.patterns, ", params commands:")
		for _, command := range routing.commands {
			fmt.Print(command.paramName, "=", command.paramValue, " ")
		}
		fmt.Println(", isReverseProxy:", routing.isReverseProxy)
	}
}

//  获取file及其最后修改时间
func getFile(name string) (*os.File, time.Time, error) {
	file, err := os.Open(name)
	if err != nil {
		return file, time.Now(), err
	}
	fileInfo, _ := file.Stat()
	t := fileInfo.ModTime()
	return file, t, nil
}

// 处理请求
func handerAll(respWriter http.ResponseWriter, req *http.Request) {
	// 检查scheme是否是http，不是则打印日志，直接返回
	// 先检查hosts文件是否有修改，如果修改过则重新加载，如果加载过程中有异常则直接返回
	if req.URL.Scheme != "http" || checkHosts() != nil {
		return
	}

	// 获得原始的url
	url := req.URL
	originUrlStr := url.String()

	// 从全局url转换映射中找到匹配的url前缀
	matchedRouting, matchedPattern := findMatchedRouting(originUrlStr)
	var matchedUrlStr, finalUrlStr string

	// 没有匹配的url转换映射，则直接反向代理；如果匹配上了则根据isReverseProxy来判读是否是反向代理
	var isReverseProxy bool = false
	if matchedRouting == nil {
		isReverseProxy = true
		finalUrlStr = originUrlStr
	} else {
		log.Printf("origin url: %s", originUrlStr)
		matchedUrlStr = strings.Replace(originUrlStr, matchedPattern, matchedRouting.path, 100)
		if matchedRouting.isReverseProxy { // 反向代理
			isReverseProxy = true
			matchedUrl, err := url.Parse(matchedUrlStr)
			if err == nil {
				for _, command := range matchedRouting.commands {
					command.do(matchedUrl)
				}
				// 得到最终的url
				url = matchedUrl
				finalUrlStr = url.String()
			}
		} else { // 资源代理
			finalUrlStr = matchedUrlStr[7:len(matchedUrlStr)]
		}
		log.Printf("final url: %s", finalUrlStr)
	}

	respWriter.Header().Set("goproxy", finalUrlStr) // 在代理请求前把最终的url设置到相应头中

	//	respWriter.Header().Set("Content-Length",strconv.FormatInt(7000, 10)) // 注意：设置相应数据的长度是无效的
	if isReverseProxy {
		reverseProxy(url, respWriter, req)
	} else {
		resourceProxy(finalUrlStr, respWriter, req)
	}
	//	respWriter.Header().Set("bbb", "2222") // 在代理完以后，对相应信息的修改无效
}

// 检查hosts文件是否修改，有修改就重新加载
func checkHosts() error {
	file, modTime, err := getFile(*hosts)
	file.Close()
	if err != nil {
		log.Panicln("[ERROR] read hosts error:", err)
		return err
	}
	if hostsVersion.Unix() != modTime.Unix() {
		routings = readHostsFile(*hosts)
	}
	return nil
}

// 根据原始的url找到匹配的url映射
func findMatchedRouting(originUrlStr string) (matchedRouting *Routing, matchedPattern string) {
	tmpStr := originUrlStr[7:len(originUrlStr)]

	//如果该url中的域名没有在hosts文件中出现过，则一定不会匹配上，直接跳过循环匹配的过程。
	domain := extractDomain(tmpStr)
	domainStatistic := domains[domain]
	if domainStatistic == nil || domainStatistic.count == 0 {
		return nil, ""
	}

	for _, index := range domainStatistic.routingsIndex {
		for _, pattern := range routings[index].patterns {
			if strings.HasPrefix(tmpStr, pattern) {
				log.Printf("matched routing is : %s %s", routings[index].path, pattern)
				return routings[index], pattern
			}
		}
	}
	return nil, ""
}

// 添加映射中指定的参数
func addParam(paramName, paramValue string, url *url.URL) {
	query := url.Query()
	if len(query.Get(paramName)) == 0 {
		query.Add(paramName, paramValue)
	} else {
		query.Set(paramName, paramValue)
	}
	url.RawQuery = query.Encode()
}

// 移除映射中指定的参数。如果只有参数名，则url中一旦出现了该参数名即删除该参数；如果既有参数名也有参数值，则只在url中存在该参数且参数值相同时才删除该参数。
func delParam(paramName, paramValue string, url *url.URL) {
	query := url.Query()
	if len(paramValue) == 0 || paramValue == query.Get(paramName) {
		query.Del(paramName)
	}
	url.RawQuery = query.Encode()
}

// 判断这个映射是否需要反向代理
func isReverseProxy(path string) bool {
	if strings.Contains(path, ":/") || strings.Contains(path, ":\\") || strings.HasPrefix(path, "/") {
		return false
	}
	return true
}

//创建一个直接的反向代理，直接用新的url替换原有request中的的url
func NewDirectReverseProxy(target *url.URL) *httputil.ReverseProxy {
	director := func(req *http.Request) {
		req.URL = target
	}
	return &httputil.ReverseProxy{Director: director}
}

// 反向代理
func reverseProxy(url *url.URL, respWriter http.ResponseWriter, req *http.Request) {
	reverseProxy := NewDirectReverseProxy(url)
	reverseProxy.ServeHTTP(respWriter, req)
}

// 静态资源代理，如果url中有参数，则移除参数部分
func resourceProxy(urlStr string, respWriter http.ResponseWriter, req *http.Request) {
	questionMark := strings.Index(urlStr, "?")
	if questionMark > -1 {
		urlStr = urlStr[0:questionMark]
	}
	http.ServeFile(respWriter, req, urlStr)
}
